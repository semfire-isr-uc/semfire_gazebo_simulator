<?xml version="1.0"?>
<sdf version='1.6'>
  <world name='default'>

    <spherical_coordinates>
      <surface_model>EARTH_WGS84</surface_model>
      <latitude_deg>47.3977</latitude_deg>
      <longitude_deg>8.54559</longitude_deg>
      <elevation>0</elevation>
      <heading_deg>0</heading_deg>
    </spherical_coordinates>
    <physics name='default_physics' default='0' type='ode'>
      <ode>
        <solver>
          <type>quick</type>
          <iters>10</iters>
          <sor>1.3</sor>
          <use_dynamic_moi_rescaling>0</use_dynamic_moi_rescaling>
        </solver>
        <constraints>
          <cfm>0</cfm>
          <erp>0.2</erp>
          <contact_max_correcting_vel>1000</contact_max_correcting_vel>
          <contact_surface_layer>0.001</contact_surface_layer>
        </constraints>
      </ode>
      <max_step_size>0.004</max_step_size>
      <real_time_factor>1</real_time_factor>
      <real_time_update_rate>250</real_time_update_rate>
    </physics>
    <scene>
      <shadows>0</shadows>
      <sky>
        <clouds/>
      </sky>
      <ambient>0.4 0.4 0.4 1</ambient>
      <background>0.7 0.7 0.7 1</background>
    </scene>
    <light name='sun' type='directional'>
      <pose frame=''>0 0 1000 0.4 0.2 0</pose>
      <diffuse>1 1 1 1</diffuse>
      <specular>0.6 0.6 0.6 1</specular>
      <direction>0.1 0.1 -0.9</direction>
      <attenuation>
        <range>20</range>
        <constant>0.5</constant>
        <linear>0.01</linear>
        <quadratic>0.001</quadratic>
      </attenuation>
      <cast_shadows>1</cast_shadows>
    </light>
    <model name='ground_plane'>
      <static>1</static>
      <link name='link'>
        <collision name='collision'>
          <pose frame=''>0 0 0 0 -0 0</pose>
          <geometry>
            <plane>
              <normal>0 0 1</normal>
              <size>250 250</size>
            </plane>
          </geometry>
          <surface>
            <friction>
              <ode>
                <mu>1</mu>
                <mu2>1</mu2>
              </ode>
              <torsional>
                <ode/>
              </torsional>
            </friction>
            <contact>
              <ode/>
            </contact>
            <bounce/>
          </surface>
          <max_contacts>10</max_contacts>
        </collision>
        <visual name='grass'>
          <pose frame=''>0 0 0 0 -0 0</pose>
          <cast_shadows>0</cast_shadows>
          <geometry>
            <mesh>
              <uri>file://grass_plane/meshes/grass_plane.dae</uri>
            </mesh>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <enable_wind>0</enable_wind>
        <kinematic>0</kinematic>
      </link>
    </model>
    <model name='the_void'>
      <static>1</static>
      <link name='link'>
        <pose frame=''>0 0 0.1 0 -0 0</pose>
        <visual name='the_void'>
          <pose frame=''>0 0 2 0 -0 0</pose>
          <geometry>
            <sphere>
              <radius>0.25</radius>
            </sphere>
          </geometry>
          <material>
            <script>
              <uri>file://media/materials/scripts/Gazebo.material</uri>
              <name>Gazebo/Black</name>
            </script>
          </material>
        </visual>
        <self_collide>0</self_collide>
        <enable_wind>0</enable_wind>
        <kinematic>0</kinematic>
      </link>
      <pose frame=''>-1000 -1000 0 0 -0 0</pose>
    </model>
    <model name='terrain'>
      <static>1</static>
      <link name='link'>
        <collision name='collision'>
          <geometry>
            <heightmap>
              <uri>model://terrain/materials/textures/heightmap.png</uri>
              <size>1024 1024 15</size>
              <pos>0 0 0.01</pos>
              <texture>
                <size>10</size>
                <diffuse>__default__</diffuse>
                <normal>__default__</normal>
              </texture>
              <blend>
                <min_height>0</min_height>
                <fade_dist>0</fade_dist>
              </blend>
            </heightmap>
          </geometry>
          <max_contacts>10</max_contacts>
          <surface>
            <contact>
              <ode/>
            </contact>
            <bounce/>
            <friction>
              <torsional>
                <ode/>
              </torsional>
              <ode/>
            </friction>
          </surface>
        </collision>
        <visual name='visual'>
          <geometry>
            <heightmap>
              <texture>
                <diffuse>file://media/materials/textures/grass_diffusespecular.png</diffuse>
                <normal>file://media/materials/textures/flat_normal.png</normal>
                <size>20</size>
              </texture>
              <texture>
                <diffuse>file://media/materials/textures/dirt_diffusespecular.png</diffuse>
                <normal>file://media/materials/textures/flat_normal.png</normal>
                <size>30</size>
              </texture>
              <texture>
                <diffuse>file://media/materials/textures/fungus_diffusespecular.png</diffuse>
                <normal>file://media/materials/textures/flat_normal.png</normal>
                <size>10</size>
              </texture>
              <blend>
                <min_height>1.5</min_height>
                <fade_dist>10</fade_dist>
              </blend>
              <blend>
                <min_height>10</min_height>
                <fade_dist>5</fade_dist>
              </blend>
              <uri>model://terrain/materials/textures/heightmap.png</uri>
              <size>1024 1024 15</size>
              <pos>0 0 0.01</pos>
            </heightmap>
          </geometry>
        </visual>
        <self_collide>0</self_collide>
        <enable_wind>0</enable_wind>
        <kinematic>0</kinematic>
      </link>
    </model>
    <gui fullscreen='0'>
      <camera name='camera'>
        <pose frame=''>50 0 15 0 0.1 3.14</pose>
        <view_controller>orbit</view_controller>
        <projection_type>perspective</projection_type>
      </camera>
    </gui>
    
    <gravity>0 0 -9.8066</gravity>
    <magnetic_field>6e-06 2.3e-05 -4.2e-05</magnetic_field>
    <atmosphere type='adiabatic'/>
    <wind/>
    
  </world>
</sdf>

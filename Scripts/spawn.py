import rospy
import time
import math
import matplotlib.pyplot as plt
import random 
import csv
import rospy
import cv2
import argparse

from scipy.spatial.transform import Rotation

from os import listdir
from os.path import isfile, join , expanduser
from gazebo_msgs.srv import SpawnModel
from gazebo_msgs.srv import DeleteModel
from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import Pose


dirs= ['bush1', 'bush2', 'bush3', 'bush4', 'bush5', 'bush6', 'bush7', 'bush8', 'bush9', 
       'tree_eu1', 'tree_eu3', 'tree_eu4', 'tree_eu5', 'tree_eu8', 'tree_eu9', 
       'tree_pine1', 'tree_pine3', 'tree_pine4', 'tree_pine5', 'tree_pine8', 'tree_pine9',
       'weed1', 'weed10', 'weed2', 'weed3', 'weed4', 'weed5', 'weed6', 'weed7', 'weed8', 'weed9',
       'terrain']
data = {}
dataX = []
dataY = []
dataZ = []
modelname= []
sdfs = {}
Dense = False
area = 30.0

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def doSpawn():

    for modelname in dirs:
        f = open('../Models/'+modelname+'/model.sdf','r')
        sdfs[modelname] = f.read()

    rospy.init_node('spawn_node',log_level=rospy.INFO)
    spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel)

    src = cv2.imread("../Models/terrain/materials/textures/heightmap.png")
    image = cv2.rotate(src, cv2.ROTATE_90_COUNTERCLOCKWISE) 
    HSV = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)

    center_coordinates = (((HSV.shape[0])/2),((HSV.shape[0])/2))

    rospy.wait_for_service('gazebo/spawn_sdf_model')

    dronePosition = (0,0,0) 

    filename = 'low-density.csv'

    if Dense:
        filename = 'high-density.csv'

    line_count = 0

    with open(filename) as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            line_count += 1
            
            dataX.append(float(row['x']))
            dataY.append(float(row['y']))
            r_min = 0
            r_max = 255.0
            t_min = 0
            t_max = 15.0

            dataX.append(float(row['x']))
            dataY.append(float(row['y']))
            x = int( center_coordinates[0] - round(float(row['x'])/2.0 ) )
            y = int( center_coordinates[1] - round(float(row['y'])/2.0 ) )

            m = HSV[x,y][2]
            
            row['z'] = ( (m / (r_max - r_min) ) * (t_max-t_min) + t_min   )
            data[line_count] = row

    fig, axs = plt.subplots(figsize=(10,10))

    plt.title("Count= "+str(len(data)))

    axs.scatter(dataX, dataY,s=1)
    plt.show()

    f = open(expanduser("~")+'/.gazebo/models/ground_plane/model.sdf','r')
    ground_plane_model = f.read()
    spawn_model_prox("ground_plane", ground_plane_model, "forest_models", Pose(), "world")

    spawn_model_prox('terrain', sdfs.get('terrain'), "forest_models", Pose(), "world")
    
    for i in data:
        model = data.get(i)

        modelcode = model['modelcode']
        modelname = model['modelname']
        model_pose = Pose()

        model_pose.position.x = float(model['x'])
        model_pose.position.y = float(model['y'])    
        model_pose.position.z = float(model['z'])   
        
        x1 = dronePosition[0]-area/2.0
        y1 = dronePosition[1]-area/2.0
        x2 = dronePosition[0]+area/2.0
        y2 = dronePosition[1]+area/2.0
        
        inside = (x1 < model_pose.position.x < x2) and (y1 < model_pose.position.y < y2)
        if inside:
            print ("%s\tspawned" % (modelname))
            rot = Rotation.from_euler('xyz', [0,0, random.uniform(0,math.pi)])
            rot_quat = rot.as_quat()
            model_pose.orientation.x = rot_quat[0]
            model_pose.orientation.y = rot_quat[1]
            model_pose.orientation.z = rot_quat[2]
            model_pose.orientation.w = rot_quat[3]

            spawn_model_prox(modelcode, sdfs.get(modelname), "forest_models", model_pose, "world")

        
    print ("Done")


if __name__ == "__main__":
    
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('--area', type=float, nargs='?', const=True, default=30.0, help='area of population')
    my_parser.add_argument('--dense', type=str2bool, nargs='?', const=True, default=False, help='generate dense file or not')
    args = my_parser.parse_args()
    Dense = args.dense
    area = args.area
    area+=1

    doSpawn()
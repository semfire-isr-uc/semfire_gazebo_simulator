import time
import math
import matplotlib.pyplot as plt
import random 
import csv
import numpy as np
import argparse
from scipy.spatial.transform import Rotation

bush = ['bush1', 'bush2', 'bush3', 'bush4', 'bush5', 'bush6', 'bush7', 'bush8', 'bush9']
flex = ['flax1', 'flax2', 'flax3', 'flax4', 'flax5', 'flax6', 'flax7', 'flax8', 'flax9']
tree_eu = ['tree_eu1', 'tree_eu3', 'tree_eu4', 'tree_eu5', 'tree_eu8', 'tree_eu9']
tree_pine = ['tree_pine1', 'tree_pine3', 'tree_pine4', 'tree_pine5', 'tree_pine8', 'tree_pine9']
weed = ['weed1', 'weed10', 'weed2', 'weed3', 'weed4', 'weed5', 'weed6', 'weed7', 'weed8', 'weed9']

data = {}
line_count = 0
dataX = []
dataY = []
dataZ = []
modelname= []
area = 100.0
Dense = True

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def doGenerate():
    filename = 'low-density.csv'

    if Dense:
        filename = 'high-density.csv'

    fig, axs = plt.subplots(figsize=(10,10))

    if Dense:
        choice_list = [0.10, -0.10 , 0.15, -0.15, 0.20, -0.20,  0.25, -0.25, 0.30, -0.30 ,0.35, -0.35, 0]
    else:
        choice_list = [0.25, -0.25, 0.70, -0.70 ,0.5, -0.5, 0] 

    if Dense:
        tree_eu_total_count =1400
        tree_pine_total_count =1250
    else:
        tree_eu_total_count =900
        tree_pine_total_count =450

    tree_total_count =(tree_eu_total_count + tree_pine_total_count) / 2

    tree_eu_ratio = tree_eu_total_count / (tree_eu_total_count + tree_pine_total_count)
    tree_pine_ratio = tree_pine_total_count / (tree_eu_total_count + tree_pine_total_count)

    tree_eu_count =0
    tree_pine_count =0
    step = int(10000 / ((tree_eu_total_count / 2.0 + tree_pine_total_count / 2.0)  / 33.3))

    irange = np.arange(0,10000,step)
    jrange = np.arange(0,10000,300)
    for ii in irange:
        for jj in jrange:        
            x = ii/100.0-50.0
            y = jj/100.0-50.0
            # print (str(x)+' '+str(y))
            x += random.choice(choice_list)
            z = 0.0
            modelname =''
            randomchoice = random.randint(0,10)
            if randomchoice % 2 ==0:
                if tree_eu_count < tree_eu_ratio * tree_total_count:
                    tree_eu_count+=1
                    modelname = random.choice(tree_eu)
                elif tree_pine_count< tree_pine_ratio * tree_total_count:
                    tree_pine_count+=1
                    modelname =random.choice(tree_pine)
            else:
                if tree_pine_count< tree_pine_ratio * tree_total_count:
                    tree_pine_count+=1
                    modelname =random.choice(tree_pine)
                elif tree_eu_count < tree_eu_ratio * tree_total_count:
                    tree_eu_count+=1
                    modelname = random.choice(tree_eu)

            flag = 1
            if len(data) >= tree_eu_count+tree_pine_count:
                flag = 0

            if flag:
                print (len(data))
                data[len(data)+1] = {'x' : x ,'y' : y, 'z': z, 'modelname' :modelname,'modelcode':'Model_'+str(len(data)+1)}
                dataX.append(x)
                dataY.append(y)

                # plt.title("Count= "+str(len(data)))
                # if len(data) % 50 == 0:
                #     axs.scatter(dataX, dataY,s=1)
                #     plt.pause(0.05)

    treeCount =len(data)
    bushcount = treeCount * 0.45
    herbcount = treeCount * 0.45
        
    while len(data) < treeCount+bushcount+herbcount :
        x = round(random.uniform(-area/2.0, area/2.0),3)
        y = round(random.uniform(-area/2.0, area/2.0),3)
        z = 0.0
        modelname =''
        if len(data) < treeCount+bushcount:
            modelname ='bush'+str(random.randrange(1,9))
        else:
            modelname ='weed'+str(random.randrange(1,9))

        flag = 1
        for i in data:
            model = data.get(i)
            d = math.sqrt( math.pow(model['x'] - x , 2) + math.pow(model['y'] - y , 2))
            if d < 1:
                flag = 0
                break
        if flag:
            print (len(data))
            data[len(data)+1] = {'x' : x ,'y' : y, 'z': z, 'modelname' :modelname,'modelcode':'Model_'+str(len(data)+1)}
            dataX.append(x)
            dataY.append(y)
            # plt.title("Count= "+str(len(data)))
            # if len(data) % 200 == 0:
            #     axs.scatter(dataX, dataY,s=1)
            #     plt.pause(0.05)
    with open(filename, 'w+') as csv_file:
        fieldnames = ['x', 'y', 'z','modelname','modelcode']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

        for line in data:
            writer.writerow(data.get(line))

    plt.title("Count= "+str(len(data)))

    axs.scatter(dataX[0:treeCount], dataY[0:treeCount],s=1,color='blue', label='Trees')
    axs.scatter(dataX[treeCount:len(data)], dataY[treeCount:len(data)],s=1,color='red', label='Bushes')
    legend = axs.legend(loc='upper center', shadow=True, fontsize='x-large')

    plt.show()


if __name__ == "__main__":
    
    my_parser = argparse.ArgumentParser()
    my_parser.add_argument('--dense', type=str2bool, nargs='?', const=True, default=False, help='generate dense file or not')
    args = my_parser.parse_args()

    Dense = args.dense

    doGenerate()
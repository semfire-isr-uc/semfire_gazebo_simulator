# SEMFIRE Gazebo Simulator

## v0.1 (Ali Ahmadi)


## 1. Introduction
This repository is including the [python scripts](https://bitbucket.org/semfire-isr-uc/semfire_gazebo_simulator/src/master/Scripts/) to prepare .world files for usage in gazebo simulation as well as [sample generated world files](https://bitbucket.org/semfire-isr-uc/semfire_gazebo_simulator/src/master/Worlds/).
### 1.1. World files
A world file contains all the elements in a Gazebo simulation, including lights, physics, and static/dynamic objects. This file is formatted using SDF ([Simulation Description Format](http://gazebosim.org/sdf.html)). If you want to know more about world files please read [this tutorial](http://gazebosim.org/tutorials?cat=build_world). Although some simple world files are included in the MRS UAV system, these world files are generated for simulation, including the uneven terrain, trees, and bushes according to the specifications in the SEMFIRE project.

These files are tested with Gazebo version 9.16.0 installed on Ubuntu 18.04 LTS 64-bit including ROS Melodic

## 2. Installation

Copy the Models directory to .gazebo directory

```bash
cp -r Models ~/.gazebo
echo 'export GAZEBO_MODEL_PATH=~/.gazebo/Models:$GAZEBO_MODEL_PATH' >> ~/.bashrc
```
## 3. Usage

change to Worlds directory then run Gazebo with the corresponding world file

```bash
cd Worlds
gazebo low-dense-forest-30by30.world
```
### 3.1. World generator
To generate world files, There are two scripts inside Scripts directory which can be used as follows.
```bash
cd Scripts
python3 generate.py # generates low density CSV file
python3 generate.py --dense # generates high density CSV file
```
After running these codes, two CSV files are generated inside Scripts directory (the directory running the script).
To populate the world inside Gazebo, do as follow.

First, Install [gazebo-ros](http://wiki.ros.org/gazebo_ros) package if you didn't.

```bash
sudo apt install ros-melodic-gazebo-ros
```

Then, load an empty world.

```bash
roslaunch gazebo_ros empty_world.launch
```

Then run one of the following commands to start spawning models inside the empty world. Depend on your PC resources, wait for spawning to be done. After that, from "File" menu, choose "Save world as" to save the world into a file.

```bash
python3 spawn.py # start spawning models with default values : area = 30 meters , low-density CSV file
python3 spawn.py --area 10 # start spawning models with default values : area = 10 meters , low-density CSV file
python3 spawn.py --area 10 --dense # start spawning models with default values : area = 10 meters , high-density CSV file
```


### 3.2. Heightmap png file generation 
The heightmap.png file is a grayscale file which each pixels intensity represents the height of the terrain inside gazebo.
0 means 0 meter and 255 means the maximum height which is assigned inside terrain model description file which is 15 meters.
The dimention of the png file should be (2^n)+1 , 1 < n < 10 for both height and width.

## 4. Troubleshooting

Regarding the "TIFFFieldWithTag" erros on terminal, and as it is mentioned [here](https://github.com/PR2/pr2_common/issues/257#issuecomment-491566771), the TIFF file inside textures directory should be converted to PNG files and respectively update material files which inlcude the TIFF file address within.
